using Newtonsoft.Json;

namespace WarheadsApiWrapper
{
    public class LaunchResponse
    {
        [JsonProperty("result")]
        public LaunchResponseResult Result;

        [JsonProperty("message")]
        public string Message;

        public LaunchResponse()
        {
        }

        public LaunchResponse(LaunchResponseResult result, string message)
        {
            this.Result = result;
            this.Message = message;
        }
    }

    public enum LaunchResponseResult
    {
        Success,
        Failure
    }
}