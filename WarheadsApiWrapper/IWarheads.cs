﻿using System.Threading.Tasks;

namespace WarheadsApiWrapper
{
    public interface IWarheads
    {
        Task<bool> IsOnline();

        Task<LaunchResponse> Launch(string launchCode);
    }
}