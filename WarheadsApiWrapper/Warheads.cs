﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace WarheadsApiWrapper
{
    public class Warheads : IWarheads
    {
        private readonly HttpClient _httpClient = new HttpClient();

        public Warheads()
        {
            this._httpClient.BaseAddress = new Uri("http://gitland.azurewebsites.net:80/");
            this._httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        /// <summary>
        /// Gets availability of API access to launch control.
        /// This will obviously never have to be used since Gitland's infrastructure always works flawlessy, comrade...
        /// </summary>
        /// <returns>True if online, otherwise false.</returns>
        public async Task<bool> IsOnline()
        {
            var response = await this._httpClient.GetAsync("api/warheads/status");
            if (!response.IsSuccessStatusCode)
            {
                return false;
            }
            
            var statusResponse = await response.Content.ReadAsAsync<StatusResponse>();

            return statusResponse.Status == OnlineStatus.Online;
        }

        /// <summary>
        /// Attemps to launch a nuke immediately.
        /// </summary>
        /// <param name="launchCode"></param>
        /// <returns></returns>
        public async Task<LaunchResponse> Launch(string launchCode)
        {
            var response = await this._httpClient.PostAsync($"api/warheads/launch?launchCode={launchCode}", null);

            var launchResponse = await response.Content.ReadAsAsync<LaunchResponse>();

            return launchResponse;
        }
    }
}
