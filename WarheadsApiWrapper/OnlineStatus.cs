﻿namespace WarheadsApiWrapper
{
    public enum OnlineStatus
    {
        Online,
        Offline
    }
}