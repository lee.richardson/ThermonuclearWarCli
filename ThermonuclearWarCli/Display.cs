﻿using System;
using System.Threading;

namespace ThermonuclearWarCli
{
    internal class Display : IDisplay
    {
        /// <summary>
        /// Output to console character by character with a delay between each
        /// and a new line at the end.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="delayMs"></param>
        public void TypeLine(string text, int delayMs = 20)
        {
            this.Type(text, delayMs);
            Console.WriteLine();
        }

        /// <summary>
        /// Output to console character by character with a delay between each.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="delayMs"></param>
        public void Type(string text, int delayMs = 20)
        {
            foreach (var ch in text)
            {
                Console.Write(ch);
                Thread.Sleep(delayMs);
            }
        }
    }
}