﻿namespace ThermonuclearWarCli
{
    internal interface ICommand
    {
        string Name { get; }
        void Execute();
    }
}
