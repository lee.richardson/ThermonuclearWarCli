﻿namespace ThermonuclearWarCli
{
    interface IDisplay
    {
        void TypeLine(string text, int delayMs = 20);
        void Type(string text, int delayMs = 20);
    }
}