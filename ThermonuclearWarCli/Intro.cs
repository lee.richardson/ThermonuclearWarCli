using System;
using System.Threading;

namespace ThermonuclearWarCli
{
    class Intro
    {
        private IDisplay Display { get; }

        public Intro(IDisplay display)
        {
            this.Display = display;
        }

        void WoprSays(string message)
        {
            Display.TypeLine(message.ToUpper());
            Console.WriteLine();
            Thread.Sleep(1000);
        }

        void HumanSays(string message)
        {
            Display.TypeLine(message, 100);
            Display.TypeLine("");
            Display.TypeLine("");
            Thread.Sleep(1000);
        }

        public void RunIntro()
        {
            Console.WriteLine("Meanwhile, in 1983...");
            Thread.Sleep(2000);

            Console.Clear();

            WoprSays("GREETINGS PROFESSOR FALKEN.");

            HumanSays("Hello.");

            WoprSays("HOW ARE YOU FEELING TODAY?");

            HumanSays("I'm fine. How are you?");

            WoprSays("EXCELLENT. IT'S BEEN A LONG TIME. CAN YOU EXPLAIN\n" +
                     "THE REMOVAL OF YOUR USER ACCOUNT ON 6 / 23 / 73 ?");

            HumanSays("People sometimes make mistakes.");

            WoprSays("YES THEY DO. SHALL WE PLAY A GAME?");

            HumanSays("Love to. How about Global Thermonuclear War?");

            WoprSays("WOULDN'T YOU PREFER A GOOD GAME OF CHESS?");

            HumanSays("Later. Let's play Global Thermonuclear War.");

            WoprSays("FINE.");
        }
    }
}