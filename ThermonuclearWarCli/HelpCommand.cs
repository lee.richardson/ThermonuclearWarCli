using System;
using System.Collections.Generic;
using System.Linq;

namespace ThermonuclearWarCli
{
    /// <summary>
    /// Displays help text.
    /// </summary>
    internal class HelpCommand : ICommand
    {
        private IDisplay Display { get; }

        public string Name { get; } = "help";
        public IList<ICommand> CommandList { get; set; }

        public HelpCommand(IDisplay display)
        {
            this.Display = display;
        }

        public void Execute()
        {
            var availableCommands = CommandList
                .Select(cmd => cmd.Name.ToUpper());

            var availableCommandsString = string.Join(", ", availableCommands);

            Display.TypeLine($"AVAILABLE COMMANDS: {availableCommandsString}");
            Display.TypeLine("AWAITING FIRST STRIKE COMMAND");
            Console.WriteLine("-----------------------------");
        }
    }
}