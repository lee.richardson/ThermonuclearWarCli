﻿namespace ThermonuclearWarCli
{
    /// <summary>
    /// Displays API status.
    /// </summary>
    internal class StatusCommand : ICommand
    {
        private ILaunchControlService LaunchControl { get; }
        private IDisplay Display { get; }

        public string Name { get; } = "status";

        public StatusCommand(ILaunchControlService launchControl, IDisplay display)
        {
            LaunchControl = launchControl;
            Display = display;
        }


        public void Execute()
        {
            this.LaunchControl.IsOnline()
                .Match(
                    result => { Display.TypeLine(result ? "ONLINE" : "OFFLINE"); },
                    () => Display.TypeLine("ERROR"));
        }
    }
}