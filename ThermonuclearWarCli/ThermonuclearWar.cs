﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ThermonuclearWarCli
{
    internal class ThermonuclearWar
    {
        private IDisplay Display { get; } = new Display();
        private bool IsRunning { get; set; } = true;

        private ILaunchControlService LaunchControl { get; }
            = new LaunchControlService(new WarheadsApiWrapper.Warheads());

        static void Main(string[] args)
        {
            new ThermonuclearWar().Go(args);
        }

        public void Go(string[] args)
        {
            Console.CursorSize = 100;
            Console.BufferHeight = Console.WindowHeight;
            Console.Clear();

            // ReSharper disable once SimplifyLinqExpression
            if (!args.Any(arg => arg == "--no-intro"))
            {
                new Intro(Display).RunIntro();
            }

            Console.Clear();

            var commandList = new List<ICommand>
            {
                new StatusCommand(LaunchControl, Display),
                new LaunchCommand(LaunchControl, Display),
                new ExitCommand(Display, () => this.IsRunning = false)
            };
            var helpCommand = new HelpCommand(Display);
            commandList.Add(helpCommand);

            commandList = commandList
                .OrderBy(cmd1 => cmd1.Name)
                .ToList();
            helpCommand.CommandList = commandList;

            helpCommand.Execute();
            while (IsRunning)
            {
                Console.Write("> ");
                var inputString = Console.ReadLine();
                Console.WriteLine();

                var toExecute = commandList
                    .FirstOrDefault(cmd => string.Equals(
                        cmd.Name,
                        inputString,
                        StringComparison.InvariantCultureIgnoreCase));

                if (inputString == null || toExecute == null)
                {
                    Display.TypeLine("SYNTAX ERROR");
                    Console.WriteLine();
                    Console.WriteLine();

                    continue;
                }

                try
                {
                    toExecute.Execute();
                }
                catch (Exception)
                {
                    Display.TypeLine("ERROR");
                }

                Console.WriteLine();
                Console.WriteLine();
            }
        }
    }
}