﻿using System;
using Optional;
using WarheadsApiWrapper;

namespace ThermonuclearWarCli
{
    internal class LaunchControlService : ILaunchControlService
    {
        public string LaunchCode { get; } = "NICEGAMEOFCHESS";
        private DateTime LastLaunch { get; set; } = DateTime.MinValue;

        public IWarheads Api { get; }

        public LaunchControlService()
        {
            this.Api = new Warheads();
        }

        public LaunchControlService(IWarheads api)
        {
            Api = api;
        }

        public Option<bool> IsOnline()
        {
            var onlineCall = Api.IsOnline();

            if (!onlineCall.Wait(2000))
            {
                return Option.None<bool>();
            }

            var isOnline = onlineCall.Result;
            return isOnline.Some();
        }

        public Option<LaunchResponse> Launch()
        {
            if (DateTime.Now < this.LastLaunch.AddMinutes(5))
            {
                const string message = "Less than 5 minutes have passed since last launch";

                return new LaunchResponse(LaunchResponseResult.Failure, message).Some();
            }

            var launchCode = this.GenerateLaunchCode();
            var launchCall = this.Api.Launch(launchCode);

            if (!launchCall.Wait(5000))
            {
                return Option.None<LaunchResponse>();
            }

            var launchResponse = launchCall.Result;
            if (launchResponse.Result == LaunchResponseResult.Success)
            {
                this.LastLaunch = DateTime.Now;
            }

            return launchCall.Result.Some();
        }

        private string GenerateLaunchCode()
        {
            var launchCode = $"{DateTime.Now:yyMMdd}-{this.LaunchCode}";

            return launchCode;
        }
    }
}